<?php
/**
 * Template part for displaying a post
 *
 * @package eportfolio
 */

namespace WP_Rig\WP_Rig;
use WP_Query;

eportfolio()->print_styles( 'eportfolio-carousel' );

wp_enqueue_script( 'jquery' );
wp_enqueue_script( 'jquery', 'http://code.jquery.com/jquery-migrate-1.2.1.min.js' );
wp_enqueue_script( 'slick', get_stylesheet_directory_uri() . '/assets/js/vendor/slick.min.js' );

wp_enqueue_script( 'carousel', get_stylesheet_directory_uri() . '/assets/js/carousel.min.js' );

if ( $args['post_type'] ) {
  //echo $args['post_type'];
}

$args = array(
	'post_type' => $args['post_type'],
	'post_status' => 'publish',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC',
);

$loop = new WP_Query( $args );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry' ); ?>>

	<div class="carousel">
		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

		<div class="carousel-card">
		<style>
    	a { color: #000000; } /* CSS link color */
  	</style>
		<a href="<?php the_permalink();?>"> <?php the_title(); ?> </a>
		<?php the_post_thumbnail( $post->ID, 'medium', array( 'class' => 'card-image' ) ); ?>
  		</div>

		  <?php endwhile; // end of the loop. ?>

	</div>




	<?php
		wp_reset_postdata();
	?>
</article>
