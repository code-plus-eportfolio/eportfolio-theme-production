<?php
/**
 * Template part for displaying the footer info
 *
 * @package eportfolio
 */

namespace WP_Rig\WP_Rig;

?>

<div class="site-info">
<footer id="footer">
	<div class="container">

			<a href="<?php the_field('profile_linkedin_url','option');?>">
			<i class="fab footer-icon fa-2x fa-linkedin-in"></i>

			<a href="<?php the_field('profile_github_url','option');?>">
			<i class="fab footer-icon fa-2x fa-github"></i>

			<a href="<?php the_field('profile_facebook_url','option');?>">
			<i class="fab footer-icon fa-2x fa-facebook"></i>

			<a href="<?php the_field('profile_instagram_url','option');?>">
			<i class="fab footer-icon fa-2x fa-instagram"></i>

			<form method="get" action="/contact" id="contact-button"><button class="contact-button">Contact Me</button></form>

	</div>



  </footer>

</div><!-- .site-info -->
