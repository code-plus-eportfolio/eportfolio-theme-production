<?php
/**
 * Template part for displaying the header navigation menu
 *
 * @package eportfolio
 */

namespace WP_Rig\WP_Rig;
?>

<nav id="site-navigation" class="main-navigation nav--toggle-sub nav--toggle-small" aria-label="<?php esc_attr_e( 'Main menu', 'eportfolio' ); ?>"
	<?php
	if ( eportfolio()->is_amp() ) {
		?>
		[class]=" siteNavigationMenu.expanded ? 'main-navigation nav--toggle-sub nav--toggle-small nav--toggled-on' : 'main-navigation nav--toggle-sub nav--toggle-small' "
		<?php
	}
	?>
>
	<?php
	if ( eportfolio()->is_amp() ) {
		?>
		<amp-state id="siteNavigationMenu">
			<script type="application/json">
				{
					"expanded": false
				}
			</script>
		</amp-state>
		<?php
	}
	?>

	<button class="menu-toggle" aria-label="<?php esc_attr_e( 'Open menu', 'eportfolio' ); ?>" aria-controls="primary-menu" aria-expanded="false"
		<?php
		if ( eportfolio()->is_amp() ) {
			?>
			on="tap:AMP.setState( { siteNavigationMenu: { expanded: ! siteNavigationMenu.expanded } } )"
			[aria-expanded]="siteNavigationMenu.expanded ? 'true' : 'false'"
			<?php
		}
		?>
	>
		<?php esc_html_e( 'Menu', 'eportfolio' ); ?>
	</button>

	<div class="primary-menu-container">
		<div class= "duke-logo"> <img src="https://www.seekpng.com/png/full/879-8795736_duke-logo-png-duke-university-logo-white.png" alt="duke logo" id="duke-pic"> </div>
		<?php eportfolio()->display_primary_nav_menu( array( 'menu_id' => 'primary-menu' ) ); get_search_form();  ?>
		<div style="height:60px ; width:60px ; position:relative ; top:15px ; right:0 ; margin-left: 15px"> <?php the_custom_logo(); ?> </div> 
	</div>
</nav><!-- #site-navigation -->
