<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eportfolio
 */

namespace WP_Rig\WP_Rig;

if ( ! eportfolio()->is_primary_sidebar_active() ) {
	return;
}

eportfolio()->print_styles( 'eportfolio-sidebar', 'eportfolio-widgets' );

?>
<aside id="secondary" class="primary-sidebar widget-area">
	<h2 class="screen-reader-text"><?php esc_attr_e( 'Asides', 'eportfolio' ); ?></h2>
	<?php eportfolio()->display_primary_sidebar(); ?>
</aside><!-- #secondary -->
