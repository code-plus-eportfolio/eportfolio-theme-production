<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eportfolio
 */

namespace WP_Rig\WP_Rig;

get_header();
wp_enqueue_script( 'gallery', get_stylesheet_directory_uri() . '/assets/js/gallery.min.js' );

eportfolio()->print_styles( 'eportfolio-content' );

?>
    <main id="primary" class="site-main">
    <?php 
$images = get_field('gallery_images','option');
$size = 'medium'; // (thumbnail, medium, large, full or custom size)
$x = 0;
if( $images ): 
    echo '<h1 class="page-title">Gallery</h1>';
?>
    <div class="gallery-page">
    <?php foreach( $images as $image ): ?>
    <div class="gallery-pic-box">
    <img id="myImg" class="gallery-pic" alt="<?php echo esc_html($image['description'])?>" src="<?php echo esc_url($image['sizes']['large']); ?>"  />
    <?php echo '<b>';
    echo $image['title'];
    echo '</b><br>';
    echo $image['caption']; ?>
    </div>
    <!-- The Modal -->
        <div id="myModal" class="modal">
            <span class="close">&times;</span>
            <img class="modal-content" id="img01">
            <div id="caption"></div>
        </div>
     <?php endforeach; ?>
    </div>
<?php endif; ?>
    </main><!-- #primary -->
<?php

get_footer();

?>


